//
//  ShopContentView.swift


import SwiftUI
import Combine

struct ActivitiesItem {
    var id: Int
    var activityName: String
    var activityNameLabel: String
    var activityImage: String
    var selectedActivity: Bool
}

struct ActivitiesPlaces {
    var id: Int
    var activityPlace: String
    var activityPlaceImage: String
    var famousPointsArray: [ActivitiesFamousPoints]
   
}

struct ActivityResource {
    var id: Int
    var resourceName: String
    var resourceDescription: String
    var resources : [ActivityResourcesItem]
}

struct ActivityResourcesItem {
    var id: Int
    var resourceName: String
    var resourceImage: String
    var resourceDescription: String
}

struct ActivitiesData {
    var id: Int
    var activitiesPlaces: [ActivitiesPlaces]
    var activityResources: [ActivityResource]
}

struct ActivitiesFamousPoints {
    var id: Int
    var pointName: String
    var pointImage: String
    var pointDescription: String
}

class Activities: ObservableObject {
    let objectWillChange = PassthroughSubject<Void, Never>()
    
    var activitiesCollection : [ActivitiesData] {
       willSet {
            objectWillChange.send()
        }
    }
    
    var activities: [ActivitiesItem] {
        willSet {
                   objectWillChange.send()
               }
    }
    
    init(data: [ActivitiesData], items: [ActivitiesItem] ) {
        self.activitiesCollection = data
        self.activities = items
    }
}

class ActivitySelected: ObservableObject {
    @Published var index: Int = 0
}

struct ActivitiesContentView: View {

    @ObservedObject var activtiesData : Activities
    @ObservedObject var selectedActivity = ActivitySelected()
    @State var isShowing: Bool = false
    @State var placeItemSelected: ActivitiesPlaces? = nil
    
    var body: some View {
        GeometryReader { g in
            ScrollView{
                    VStack(alignment: .leading) {
                        
                        Text("Activities")
                        .font(.system(size: 20))
                        .padding(.leading, 30)
                        .padding(.top, 10)
                        
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack (spacing: 10){
                                ForEach(self.activtiesData.activities, id: \.id) { item in
                                    ShopPromotionBannerView(activtiesItems: item, selectedActivity: self.selectedActivity)
                                            .frame(width: 120, height: 100)
                                }
                            }.padding(.leading, 30)
                            .padding(.trailing, 30)
                            .padding(.bottom, 10)
                        }
                        .padding(.top, 20)
                        Spacer()
         
                        
                        ScrollView(.horizontal, showsIndicators: false) {

                  ZStack{
                  
                    Button(action: {
                       print("hi")
                    }) {
                    Image("surfing").renderingMode(.original)
                           .resizable()
                           .frame(width: 355, height: 225)
                           .background(Color.black)
                           .cornerRadius(10)
                           .opacity(0.8)
                        .aspectRatio(contentMode: .fill).padding(.leading, 30)
                        
                    }
                  
                   VStack (alignment: .leading) {
                       Spacer()
                       
                       Text("Weekly report")
                           .foregroundColor(Color.white)
                           .font(.system(size: 20, weight: .bold, design: Font.Design.default))
                           .padding(.bottom, 24)
                   }
                       
                   
               }
               .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
               .background(Color.white)
             .frame(width: 355, height: 225)
                                
                            }
                        
                        Text("Analytics")
                            .font(.system(size: 20))
                            .padding(.leading, 30)
                            .padding(.top, 10)
                        
                          ScrollView(.horizontal, showsIndicators: false) {
                        ZStack{
                            
                            HStack(alignment: .bottom, spacing: 8){
                                ForEach(percents){i in
                                    Bar(percent: i.percent, day:i.day)
                                }
                            }.padding(.leading, 30)
                            Spacer()
 
                        }
                        
                        }  .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                          .background(Color.white)
                        .frame(width: 355, height: 225)
                        
                    }
            }
        }
    }
}



struct ShopPromotionBannerView: View {
    var activtiesItems: ActivitiesItem
    @ObservedObject var selectedActivity: ActivitySelected
    
    var body: some View {
        
        Button(action: {
            self.selectedActivity.index = self.activtiesItems.id
            
        }) {
            GeometryReader { g in
                   ZStack{
                    Image("\(self.activtiesItems.activityImage)").renderingMode(.original)
                       .resizable()
                       .opacity(0.8)
                       .aspectRatio(contentMode: .fill)
                       .background(Color.black)
                    
                    
                    if (self.selectedActivity.index == self.activtiesItems.id) {
                           Text("✓ \(self.activtiesItems.activityName)")
                                    .font(.system(size: 14, weight: .bold, design: Font.Design.default))
                                    .foregroundColor(Color.white)
                    } else {
                             Text(self.activtiesItems.activityName)
                                    .font(.system(size: 14, weight: .bold, design: Font.Design.default))
                                     .foregroundColor(Color.white)
                    }
                               
                   }.frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                   .cornerRadius(15)
               }
        }
    }
    
    
    }


//barchart
struct Bar: View{
    var percent: CGFloat=0
    var day=" "
    
    var body: some View{
        VStack{
            Text(String(format:"%.0f", Double(percent))+"%").foregroundColor(Color.black.opacity(0.5))
            
            Rectangle().fill(Color(UIColor(red: 0.09, green: 0.73, blue: 0.77, alpha: 1.00))).frame(width: UIScreen.main.bounds.width/3-12, height:getHeight())
            
            Text(day).foregroundColor(Color.black.opacity(0.5))
        }
    }
    
    func getHeight()->CGFloat{
        return 200/100 * percent
    }
}



//Sample Data

struct type: Identifiable{
    var id:Int
    var percent: CGFloat
    var day: String
}

let defaultdata = UserDefaults.standard

var notifications = defaultdata.integer(forKey: "notification")
var news = defaultdata.integer(forKey: "news")
var recognition = defaultdata.integer(forKey: "recognition")

var percents=[
    type(id:0,percent:CGFloat(notifications),day:"Reminders"),
    type(id:1,percent:CGFloat(news),day:"Read"),
    type(id:2,percent:CGFloat(recognition),day:"Id")]

    



struct ActivitiesContentView_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
