//
//  CardView.swift
//  MentalHealthArticles
//
//  Created by Dany on 18/10/20.
//

import SwiftUI
import SwiftyJSON
import SDWebImageSwiftUI


struct dataType: Identifiable{
    
    var id:String
    var title:String
    var desc:String
    var url: String
    var image: String
    var content: String
}

struct CardView: View {
    
    @ObservedObject var list = getData()
    
    var body: some View {
        NavigationView{
            List(list.datas) { article in
                NavigationLink(destination: DetailArticle(article: article)) {
                    VStack{
                        WebImage(url: URL(string:article.image)!, options: .highPriority, context:nil)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                        
                        HStack{
                            VStack(alignment: .leading) {

                                Text(article.title)
                                    .font(.title)
                                    .fontWeight(.black)
                                    .foregroundColor(.primary)
                                    .lineLimit(3)
                                Text(article.desc)
                                    .font(.caption)
                                    .foregroundColor(.secondary)
                            }
                            .layoutPriority(100)
                            Spacer()
                        }
                        .padding()
                    }
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10)
                                .stroke(Color(.sRGB, red:10/255,green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1))
                    .padding([.top, .horizontal])
                }
            }
            .navigationBarTitle("Articles", displayMode: .inline)
            .background(NavigationConfigurator { nc in
                nc.navigationBar.barTintColor = .white
                nc.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.black]
            })
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct DetailArticle: View {
    let article: dataType
    var body: some View {
        
        ScrollView{
            VStack{
                WebImage(url: URL(string:article.image)!, options: .highPriority, context:nil)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                HStack{
                    VStack(alignment: .leading) {

                        Text(article.title)
                            .font(.title)
                            .fontWeight(.black)
                            .foregroundColor(.primary)
                            .lineLimit(3)
                        Text(article.desc)
                            .font(.caption)
                            .foregroundColor(.secondary)
                            .padding(.bottom, 20)
                        Text(article.content)
                            .font(.headline)
                            .foregroundColor(.secondary)
                    }
                    .layoutPriority(100)
                    Spacer()
                }
            }
            .cornerRadius(10)
            .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(.sRGB, red:10/255,green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1))
        }
    }
}

struct NavigationConfigurator: UIViewControllerRepresentable {
    var configure: (UINavigationController) -> Void = { _ in }

    func makeUIViewController(context: UIViewControllerRepresentableContext<NavigationConfigurator>) -> UIViewController {
        UIViewController()
    }
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<NavigationConfigurator>) {
        if let nc = uiViewController.navigationController {
            self.configure(nc)
        }
    }

}




class getData: ObservableObject{
    @Published var datas = [dataType]()
    
    init(){
        let source = "https://newsapi.org/v2/top-headlines?q=health&apiKey=03867bee8e3f4df4ba0e097f3f90e0b3"
        let url = URL(string: source)!
        let session = URLSession(configuration: .default)
        session.dataTask(with: url){
            (data, _, err) in
            if err != nil{
                print((err?.localizedDescription)!)
                return
            }
            
            let json = try! JSON(data: data!)
            for i in json["articles"]{
                let title = i.1["title"].stringValue
                let description = i.1["description"].stringValue
                let url = i.1["url"].stringValue
                let image = i.1["urlToImage"].stringValue
                let id = i.1["publishedAt"].stringValue
                let content = i.1["content"].stringValue
                
                DispatchQueue.main.async{
                    if !image.isEmpty{
                        self.datas.append(dataType(id:id, title: title, desc:description, url:url, image: image, content: content))}
                }
            
            }

            DispatchQueue.global(qos: .userInitiated).async {
                var count = 0;
                for item in self.datas{
                    sharedInstance.setObject(item as AnyObject, forKey: String(count) as NSString)
                    count = count+1;
                    print(count)
                }
        }
    }.resume()
}
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView()
    }
}
