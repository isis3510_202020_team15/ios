//
//  DashboardViewController.swift
//  MentalHealth
//
//  Created by DAM on 17/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SwiftUI

class DashboardViewController: UIViewController {
    

        @IBOutlet var Welcome: UIView!
        @IBOutlet weak var welcomelabel: UILabel!
        

    @IBOutlet weak var Subview: UIView!
    @IBOutlet weak var Hstack: UIStackView!
        @IBOutlet weak var VStack: UIStackView!
        @IBOutlet weak var Tabbar: UITabBar!

    @IBOutlet var ButDash: UIButton!

    @IBOutlet var ButLoc: UIButton!
    
    private let backgroundColor: UIColor = .white
        private let tintColor = UIColor(hexString: "#16BAC5")
        private let subtitleColor = UIColor(hexString: "#464646")
     

        private let buttonFont = UIFont.boldSystemFont(ofSize: 20)
        
        private let titleFont = UIFont.boldSystemFont(ofSize: 30)
    
    
    
        struct ActivitiesItem {
            var id: Int
            var activityName: String
            var activityNameLabel: String
            var activityImage: String
            var selectedActivity: Bool
        }
    
        static let activities: [ActivitiesItem] = [
            ActivitiesItem(id: 0, activityName: "SURFING", activityNameLabel: "Surfing", activityImage: "surfing", selectedActivity: false),
            ActivitiesItem(id: 1, activityName: "SNOWBOARD", activityNameLabel: "Snowboarding", activityImage: "snowboarding", selectedActivity: false),
            ActivitiesItem(id: 2, activityName: "HIKING", activityNameLabel: "Hiking", activityImage: "hiking", selectedActivity: false)
            
        ]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        view.backgroundColor = HelperDarkMode.mainThemeBackgroundColor
        
        welcomelabel.font = titleFont
        welcomelabel.text = "Dashboard"
        welcomelabel.textColor = HelperDarkMode.mainTextColor
    
        ButLoc.setTitle("Options", for: .normal)
        ButLoc.addTarget(self, action: #selector(nextController), for: .touchUpInside)
        ButLoc.configure(color: backgroundColor,
            font: buttonFont,
            cornerRadius: 40/2,
             backgroundColor: UIColor(hexString: "#5863F8"))
        
        let childView = UIHostingController(rootView: ActivitiesContentView(activtiesData: Activities(data: ActivitiesMockStore.activityData, items: ActivitiesMockStore.activities)))
        addChild(childView)
        childView.view.frame = Subview.bounds
        Subview.addSubview(childView.view)
        childView.didMove(toParent: self)
        
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc private func nextController() {
          let optionsVC = OptionsViewController(nibName:"OptionsViewController", bundle:nil)
          self.navigationController?.pushViewController(optionsVC, animated: true)

      }
    
}
