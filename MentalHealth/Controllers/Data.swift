//
//  Data.swift
//  MentalHealthArticles
//
//  Created by Dany on 18/10/20.
//

import SwiftUI

struct Post: Decodable, Identifiable {
    let id = UUID()
    var title: String
    var body: String
}

struct ArticleData: Codable, Identifiable{
    let id = UUID()
    var image: String
    var category: String
    var heading: String
    var author: String
    var content: String
}

class Api{
    func getArticles() {
        let url = URL(string: "https://mental-health-articles-api.firebaseio.com/article1.json")
        
        URLSession.shared.dataTask(with: url!) { (data, _, _) in
            let posts = try! JSONDecoder().decode(ArticleData.self, from: data!)
            print(posts)
        }
        .resume()
    }
    func getPosts() {
        print("hola")
        let url = URL(string: "https://jsonplaceholder.typicode.com/posts")
        
        URLSession.shared.dataTask(with: url!) { (data, _, _) in
            let posts = try! JSONDecoder().decode([Post].self, from: data!)
            print(posts)
        }
        .resume()
    }
}
