//
//  NewsViewController.swift
//  MentalHealth
//
//  Created by DAM on 18/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SwiftUI

class NewsViewController: UIViewController {

    @IBOutlet weak var NewsContainer: UIScrollView!
    
    
    
    private let backgroundColor: UIColor = .white
    private let tintColor = UIColor(hexString: "#16BAC5")
    private let subtitleColor = UIColor(hexString: "#464646")
 
    
    private let titleFont = UIFont.boldSystemFont(ofSize: 30)
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if NetworkMonitor.shared.isConnected{

            let childView = UIHostingController(rootView: CardView())
                
            addChild(childView)
            childView.view.frame = NewsContainer.bounds
            NewsContainer.addSubview(childView.view)
            childView.didMove(toParent: self)
        }
        else{

            let childView = UIHostingController(rootView: NoConnection())
                           
            addChild(childView)
            childView.view.frame = NewsContainer.bounds
            NewsContainer.addSubview(childView.view)
            childView.didMove(toParent: self)
        }

    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

struct NewsViewController_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
