//
//  NoConnection.swift
//  MentalHealth
//
//  Created by DAM on 30/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI
import SwiftyJSON
import SDWebImageSwiftUI


struct NoConnection: View {
    
    @ObservedObject var list = getDataNoConnection()
    
    var body: some View {
        VStack(alignment: .leading){
            Text("No internet connection").font(.system(size:18, weight: .light, design: .serif)).italic().frame(maxWidth: .infinity, alignment: .center)
            
        NavigationView{
                   List(list.datas) { article in
                       NavigationLink(destination: DetailArticle(article: article)) {
                           VStack{
                               WebImage(url: URL(string:article.image)!, options: .highPriority, context:nil)
                                   .resizable()
                                   .aspectRatio(contentMode: .fit)
                               
                               HStack{
                                   VStack(alignment: .leading) {

                                       Text(article.title)
                                           .font(.title)
                                           .fontWeight(.black)
                                           .foregroundColor(.primary)
                                           .lineLimit(3)
                                       Text(article.desc)
                                           .font(.caption)
                                           .foregroundColor(.secondary)
                                   }
                                   .layoutPriority(100)
                                   Spacer()
                               }
                               .padding()
                           }
                           .cornerRadius(10)
                           .overlay(RoundedRectangle(cornerRadius: 10)
                                       .stroke(Color(.sRGB, red:10/255,green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1))
                           .padding([.top, .horizontal])
                       }
                   }
                   .navigationBarTitle("Articles", displayMode: .inline)
                   .background(NavigationConfigurator { nc in
                       nc.navigationBar.barTintColor = .white
                       nc.navigationBar.titleTextAttributes = [.foregroundColor : UIColor.black]
                   })
               }
               .navigationViewStyle(StackNavigationViewStyle())
           }
        }
    }




class getDataNoConnection: ObservableObject{
    @Published var datas = [dataType]()
    
    init(){
            DispatchQueue.main.async {
                
                if(sharedInstance.object(forKey: "0")as! dataType?==nil)
                { }
                else{
                self.datas.append(sharedInstance.object(forKey: "0" as NSString) as! dataType)
                }
                if(sharedInstance.object(forKey: "1")as! dataType?==nil)
                {}
                else{self.datas.append(sharedInstance.object(forKey: "1" as NSString) as! dataType)}
                if(sharedInstance.object(forKey: "2")as! dataType?==nil)
                {}
                else{self.datas.append(sharedInstance.object(forKey: "2" as NSString) as! dataType)}
        
        }
        
    }
}



