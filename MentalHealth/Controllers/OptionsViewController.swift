//
//  OptionsViewController.swift
//  MentalHealth
//
//  Created by DAM on 18/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Firebase

class OptionsViewController: UIViewController {

    @IBOutlet var Uview: UIView!
    @IBOutlet weak var Label: UILabel!
    @IBOutlet weak var LabelOp1: UILabel!
    @IBOutlet weak var LabelOp2: UILabel!
    @IBOutlet weak var LabelOp3: UILabel!
    @IBOutlet weak var ButOp1: UIButton!
    @IBOutlet weak var ButOp2: UIButton!
    @IBOutlet weak var ButOp3: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    let defaultdata = UserDefaults.standard
    
    
    private let backgroundColor = HelperDarkMode.mainThemeBackgroundColor
    private let tintColor = UIColor(hexString:"#16BAC5")
    
    private let titleFont = UIFont.boldSystemFont(ofSize: 30)
    private let buttonFont = UIFont.boldSystemFont(ofSize: 20)
     
    private let subtitleColor = UIColor(hexString: "#464646")
    
    private let subtitleFont = UIFont.boldSystemFont(ofSize: 18)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        view.backgroundColor = backgroundColor

        Label.font = titleFont
        Label.text = "Options"
        Label.textColor = tintColor
        
        LabelOp1.font = subtitleFont
        LabelOp1.text = "Object Recognition"
        LabelOp1.textColor = subtitleColor
        
        LabelOp2.font = subtitleFont
        LabelOp2.text = "Set reminders"
        LabelOp2.textColor = subtitleColor
        
        LabelOp3.font = subtitleFont
        LabelOp3.text = "Noticias"
        LabelOp3.textColor = subtitleColor
        
        
        ButOp1.setTitle("Abrir cámara", for: .normal)
        ButOp1.addTarget(self, action: #selector(objectRecognition), for: .touchUpInside)
        ButOp1.configure(color: backgroundColor,
         font: buttonFont,
          cornerRadius: 55/2,
          backgroundColor: tintColor)
        
        
        ButOp2.setTitle("Programar", for: .normal)
        ButOp2.addTarget(self, action: #selector(nextController), for: .touchUpInside)
        ButOp2.configure(color: backgroundColor,
         font: buttonFont,
          cornerRadius: 55/2,
          backgroundColor: tintColor)
        
        ButOp3.setTitle("Ver", for: .normal)
        ButOp3.addTarget(self, action: #selector(news), for: .touchUpInside)
        ButOp3.configure(color: backgroundColor,
         font: buttonFont,
          cornerRadius: 55/2,
          backgroundColor: tintColor)
    }


    @objc func didTapBackButton() {
      self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @objc private func nextController() {
        Analytics.logEvent("SetNotification", parameters: nil)
        
        var clicks = defaultdata.integer(forKey: "notification")
        clicks = clicks + 1
        defaultdata.set(clicks, forKey: "notification")
        defaultdata.synchronize()
        
        let storyboard = UIStoryboard(name: "LocationNotifier", bundle: nil)
          let vc = storyboard.instantiateViewController(withIdentifier: "LocationNotifier")
          self.present(vc, animated: true)

      }
    
    
    @objc private func objectRecognition() {
        Analytics.logEvent("ObjectRecognition", parameters: nil)
        
        var clicks = defaultdata.integer(forKey: "recognition")
        clicks = clicks + 1
        defaultdata.set(clicks, forKey: "recognition")
        defaultdata.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let vc = storyboard.instantiateViewController(withIdentifier: "VisionObjectRecognition")
          self.present(vc, animated: true)
      }
    
    
    @objc private func news() {
      
         Analytics.logEvent("News", parameters: nil)
        
        var clicks = defaultdata.integer(forKey: "news")
        clicks = clicks + 1
        defaultdata.set(clicks, forKey: "news")
        defaultdata.synchronize()
        
        let newsVC = NewsViewController(nibName:"NewsViewController", bundle:nil)
            self.navigationController?.pushViewController(newsVC, animated: true)
  
      }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
