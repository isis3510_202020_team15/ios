//
//  TabBasrView.swift
//  MentalHealth
//
//  Created by DAM on 18/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import SwiftUI

struct TabbarView: View {
    var body: some View {
        TabView {
            NavigationView {
                ActivitiesContentView(activtiesData: Activities(data: ActivitiesMockStore.activityData, items: ActivitiesMockStore.activities))
            }
            .tag(0)
            .tabItem {
                Image("activity-1")
                    .resizable()
                Text("Activities")
            }
            
            NavigationView {
           window.rootViewController = UIHostingController(rootView: LocationViewController())
            }
            .tag(1)
            .tabItem {
                Image("shopping-cart-icon")
                Text("Cart")
            }
            
            NavigationView {
                     LocationViewController()
                  }
                   .tag(2)
                    .tabItem {
                    Image("profile-glyph-icon")
                    Text("Account")
                }
        }
    }
}
