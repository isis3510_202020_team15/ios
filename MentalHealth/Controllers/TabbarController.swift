//
//  TabbarController.swift
//  MentalHealth
//
//  Created by DAM on 18/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

class TabbarController: UITabBarController{
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabbar()
    }
    
    func setupTabbar(){
       let dashboardController = UINavigationController(rootViewController: DashboardViewController())
        
      let optionsController = UINavigationController(rootViewController: LocationViewController())
        
        viewControllers=[dashboardController,optionsController]
    }
    
    
}
