//
//  LocationNotificationSchedulerDelegate.swift
//  MentalHealth
//
//  Created by DAM on 18/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UserNotifications

protocol LocationNotificationSchedulerDelegate: UNUserNotificationCenterDelegate {
    
    /// Called when the user has denied the notification permission prompt.
    func notificationPermissionDenied()
    
    /// Called when the user has denied the location permission prompt.
    func locationPermissionDenied()
    
    /// Called when the notification request completed.
    ///
    /// - Parameter error: Optional error when trying to add the notification.
    func notificationScheduled(error: Error?)
}
