//
//  NSCache+Singleton.swift
//  MentalHealth
//
//  Created by DAM on 5/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

var sharedInstance : NSCache<NSString, AnyObject> {
    
    struct Static {
        static let instance : NSCache = NSCache<NSString, AnyObject>()
        }
        return Static.instance
    }



open class datatype: NSObject, NSCoding {
    open var title = ""
    open var desc = ""
    open var url = ""
    open var image = ""
    open var id = ""
    open var content = ""
    
    override init() {
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as! String
        self.desc = aDecoder.decodeObject(forKey: "desc") as! String
        self.url = aDecoder.decodeObject(forKey: "url") as! String
        self.image = aDecoder.decodeObject(forKey: "image") as! String
        self.id = aDecoder.decodeObject(forKey: "id") as! String
        self.content = aDecoder.decodeObject(forKey: "content") as! String
    }
    
    open func encode(with aCoder: NSCoder) {
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.desc, forKey: "desc")
        aCoder.encode(self.url, forKey: "url")
        aCoder.encode(self.image, forKey: "image")
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.content, forKey: "content")
    }
}


