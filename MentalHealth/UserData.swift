//
//  UserData.swift
//  MentalHealth
//
//  Created by DAM on 13/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI
import Combine

final class UserData: ObservableObject{

    @Published var reminders: CGFloat = 1;
    @Published var recognition: CGFloat = 1;
    @Published var news: CGFloat = 1;
    @Published var total: CGFloat = 1;
}
